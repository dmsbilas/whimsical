// Plain JavaScript codes //
var UserAppRelationObj = {
	user_logged_in_and_application_connected : false,
	user_logged_in_but_application_not_authorized : false,
	user_not_logged_in : false
};
var GlobalScope = {
	user_access_token : '',
	user_id : '',

	PostObj : {
	},

	PageObj : {
		id:'',
		data: [],
		paging:{},
		page_access_token :''
	}
};

var SeeUserAppRelation = function(){
	FB.getLoginStatus(function(loginResponse) {
		if (loginResponse.status === 'connected') {
			// the user is logged in and has authenticated your
			// app, and response.authResponse supplies
			// the user's ID, a valid access token, a signed
			// request, and the time the access token
			// and signed request each expire
			GlobalScope.user_id = loginResponse.authResponse.userID;
			GlobalScope.user_access_token = loginResponse.authResponse.accessToken;
			UserAppRelationObj.user_logged_in_and_application_connected = true;
			console.log("The user is logged in and has authenticated app");
		} else if (loginResponse.status === 'not_authorized') {
			// the user is logged in to Facebook,
			// but has not authenticated your app
			UserAppRelationObj.user_logged_in_but_application_not_authorized = true;
			console.log("The user is logged in to Facebook, but has not authenticated your app" );
		} else {
			// the user isn't logged in to Facebook.
			UserAppRelationObj.user_not_logged_in = true;
			console.log("User isn't logged in to facebook ");
		}
	});
};

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.

var checkLoginState = function(){
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
};

var ApiTest = function(){
	FB.api('/me', function(response) {
		document.getElementById('status').innerHTML =
			'<br/>Thanks for logging in, ' + response.name + '!<br/>';
		console.log('Successful login for: ' + response.name);
	});
};

var statusChangeCallback = function (response) {
	console.log('statusChangeCallback');
	console.log(response);
	// The response object is returned with a status field that lets the
	// app know the current login status of the person.
	// Full docs on the response object can be found in the documentation
	// for FB.getLoginStatus().
	if (response.status === 'connected') {
		// Logged into your app and Facebook.
		ApiTest();
	} else if (response.status === 'not_authorized') {
		// The person is logged into Facebook, but not your app.
		document.getElementById('status').innerHTML = 'Please log ' +
			'into this app.';
	} else {
		// The person is not logged into Facebook, so we're not sure if
		// they are logged into this app or not.
		document.getElementById('status').innerHTML = 'Please log ' +
			'into Facebook.<br/>';
	}
}



var listUserPages =function (element, index, array) {
	GlobalScope.PageObj.id = element.id; //page id
	GlobalScope.PageObj.page_access_token = element.access_token; // access token for the page
	document.getElementById('status').innerHTML += "<tr><td>"+ element.name + "</td> <td> "+element.id+"</td></tr>";
}

var PostDetails = function(element, index, array){
	if(element.message != 'undefined'){
		GlobalScope.PostObj.id = element.id;
		GlobalScope.PostObj.created_time = element.created_time;
		GlobalScope.PostObj.message = element.message;

		document.getElementById('status').innerHTML += "<br/>Post body:"+ element.description +" ID: "+element.id;

	}
};

var GetLikes = function(element, index, array){
	FB.api(
		element.id+'/likes',
		function (response) {
			if (response && !response.error) {
				return response.data.length;
			}
		}
	);
};

var SeePostImage = function(){
	var postID = document.getElementById('post_id').value;
	GetPhoto(postID);
};

var GetPhoto = function(postID){
// Getting Image of a post
	document.getElementById('status').innerHTML = "";
	FB.api(
		'/v2.5/'+postID+'?fields=full_picture',
		function (response) {
			if (response && !response.error) {
				document.getElementById('status').innerHTML = "<img src='"+response.full_picture+"' />";
			}else{
				console.log(response.error);
			}
		}
	);
};

var SeeUserPages = function(){
	FB.api('/me/accounts', function(response) {
		document.getElementById('status').innerHTML = "<tr><th>Name</th><th>ID</th></tr>";
		response.data.forEach(listUserPages);
	});
};

var SeePagePosts = function(pageId){
	GlobalScope.PageObj.id = pageId;
	/* make the API call */
	//?fields=created_time
	FB.api(
		GlobalScope.PageObj.id+'/feed',

		function (response) {
			if (response && !response.error) {
				console.log(response);
				GlobalScope.PageObj.data = response.data;
				GlobalScope.PageObj.paging = response.paging;
				document.getElementById('status').innerHTML = "";
				GlobalScope.PageObj.data.forEach(PostDetails);

			}else{
				console.log(response.error);
			}
		}
	);
};

var GetPageAccessToken = function(){
	GlobalScope.PageObj.id = document.getElementById("page_id").value;
	FB.api(
		GlobalScope.PageObj.id+'?fields=access_token', function(response){
			GlobalScope.PageObj.page_access_token = response.access_token;
		}
	);
};

var PostToFbPage = function(){
	var post = {};
	post.name = document.getElementById("post_name").value;
	post.description = document.getElementById("post_description").value;
	post.message = document.getElementById("post_message").value;
	post.image = document.getElementById("post_image").value;
	post.link = document.getElementById("post_link").value;

	FB.api(
		GlobalScope.PageObj.id+'/feed',
		"POST",
		{
			"name" : post.name,
			"description" : post.description,
			"message": post.message,
			"link":	post.link,
			"url" : post.image,
			"access_token" :GlobalScope.PageObj.page_access_token
		},
		function (response) {
			if (response && !response.error) {
				alert("Posted to facebook successfully");
				console.log(response);

			}
		}
	);
};

var sharedPost = {
	originalPostId :"",
	sharingUserId : "",
	sharedPostId:""
};
var allShares = [];
var getPostSharesById = function(postId){

	FB.api(
	    "/v2.3/"+postId+"?metadata=1",
	    "GET",
	    function (response) {
	      if (response && !response.error) {
	      	FB.api(
	      			response.metadata.connections.sharedposts,
	      			"GET",
	      			function(response){
	      				if(response && !response.error){
	      					for(var i= 0 ; i< response.data.length;i++){
	      						// sharedPost.originalPostId = postId;
	      						// sharedPost.sharingUserId = response.data[i].from.id;
	      						// sharedPost.sharedPostId = response.data[i].id
	      						console.log(response.data[i]);
	      						// allShares.push(sharedPost);

	      					}//end for
	      					//console.log(response);
	      				}//end if
	      			}//end api callback
	      		);//end callback
	        //console.log(response.metadata.connections.sharedposts);
	      }//end if
	    }//end callback
	);//end callback
};//end function

var CreatePageEvent = function(){
	var event = {};
	event.page_id = GlobalScope.PageObj.id;
	event.name = 'Bil event';
	event.description = 'hi dis s an event';
	event.location = 'Location name and street';
	event.city = 'chennai';
	//event.start_time = '2013-12-31T13:45:00+0100';
	event.privacy_type = 'OPEN';

	FB.api(
		'/v2.5/'+GlobalScope.PageObj.id+'/events?access_token='+GlobalScope.PageObj.page_access_token,
		"POST",
		event,
		function(response){
			console.log(response);
		}
	);
};

var createPageTab = function(pageId){
	/* make the API call */
	FB.api(
		"/"+pageId+"/call_to_actions?access_token="+GlobalScope.PageObj.page_access_token,
		"POST",
		{
			type : "SIGN_UP",
			web_destination_type : "WEBSITE",
			web_url : "www.google.com"
		},
		function (response) {
			console.log(response);
		}
	);
};


// jQuery codes //
jQuery(document).ready(function($){
	$('#btn_app_permission').on('click', function(){
		SeeUserAppRelation();
	});// End of btn_app_permission click

	$('#btn_see_user_pages').on('click', function(){
		SeeUserPages();
	});
	$('#btn_show_page_posts').on('click', function(){
		var pageId = document.getElementById("page_id").value;
		GetPageAccessToken();
		SeePagePosts(pageId);
	});

	$('#btn_show_post_img').on('click', function(){
		SeePostImage();
	});
	$('#publish_post').on('click', function(){
		GetPageAccessToken();
		PostToFbPage();
	});
	$('#show_post_shares').on('click', function(){
		var post_id = document.getElementById("post_id").value;
		GetPageAccessToken();
		getPostSharesById(post_id);
	});

	$('#create_tab').on('click', function(){
		var page_id = document.getElementById("page_id").value;
		GetPageAccessToken();
		//CreatePageEvent();
		createPageTab(page_id);
	});

	

});// End of jQuery


//FB.api(
//	"/{page-id}/feed",
//	"POST",
//	{
//		"message": "This is a test message",
//		"scheduled_publish_time": Math.round(new Date().getTime() / 1000) + 120,
//		"published": false,
//		"access_token": "{page-access-token}"
//	},
//	function (response) {
//		console.log(response);
//		if (response && !response.error) {
//			/* handle the result */
//		}
//	});







